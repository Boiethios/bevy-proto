pub mod system {
    use bevy::prelude::*;
    use bevy::render::camera::{OrthographicProjection, WindowOrigin};

    pub fn setup(mut commands: Commands, win: Res<WindowDescriptor>) {
        commands
            .spawn(Camera2dComponents {
                orthographic_projection: OrthographicProjection {
                    window_origin: WindowOrigin::BottomLeft,
                    ..Default::default()
                },
                // Change the scale when the window size changes:
                transform: Transform::from_scale(crate::W as f32 / win.width as f32),
                ..Default::default()
            })
            .spawn(UiCameraComponents::default());
    }
}
