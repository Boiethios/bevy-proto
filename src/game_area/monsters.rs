pub mod spider;

//use bevy::prelude::*;

pub struct Monster;

#[derive(serde::Deserialize, Debug)]
pub enum Type {
    Spider(spider::Spider),
}

pub mod behavior {
    use crate::game_area::time::PausableTimer;

    /// Comes from the level files.
    pub mod dto {
        #[derive(serde::Deserialize, Debug)]
        pub enum Action {
            /// Angle in degrees + distance/time limit.
            MoveFor(f32, For),
            /// How long it will stay.
            Stay(u64),
            /// Go to another index. Allows to do a loop.
            Goto(super::Index),
        }

        #[derive(serde::Deserialize, Debug, Clone)]
        pub enum For {
            /// Milliseconds.
            Duration(u64),
            /// Pixels.
            Distance(f32),
        }
    }

    #[derive(Debug, Clone)]
    pub struct Behavior {
        actions: Vec<Action>,
        index: Option<usize>,
        /// Indicates that the action targeted by the action wasn't the frame before.
        new: bool,
    }

    impl Behavior {
        pub fn new(actions: Vec<Action>) -> Self {
            let mut behavior = Behavior {
                actions,
                index: Some(0),
                new: true,
            };

            behavior.goto();

            behavior
        }

        /// Is this a new action.
        pub fn is_new(&self) -> bool {
            self.new
        }
        /// The actions isn't new anymore.
        pub fn disable_new(&mut self) {
            self.new = false;
        }

        ///// Returns the current action.
        //pub fn action(&self) -> Option<&Action> {
        //    self.index.and_then(|i| self.actions.get(i))
        //}

        /// Returns the current action (mutable).
        pub fn action_mut(&mut self) -> Option<&mut Action> {
            self.actions.get_mut(self.index?)
        }

        fn goto(&mut self) {
            fn goto(behavior: &Behavior, i: Option<usize>) -> Option<usize> {
                let mut seen = vec![false; behavior.actions.len()];
                let mut i = i?;

                while let Action::Goto(index) = behavior.actions.get(i)? {
                    if *seen.get(i)? {
                        return None;
                    }
                    seen[i] = true;
                    i = match index {
                        Index::Absolute(new) => *new,
                        Index::Relative(offset) => {
                            if offset.is_negative() {
                                i.checked_sub(offset.checked_abs()? as usize)?
                            } else {
                                i.checked_add(*offset as usize)?
                            }
                        }
                    }
                }
                Some(i)
            }

            self.index = goto(self, self.index);
        }

        /// Go to the next action (manages the Goto action).
        pub fn next(&mut self) {
            if let Some(i) = &mut self.index {
                *i += 1;
            }

            self.new = true;
            self.goto();
        }
    }

    #[derive(Debug, Clone)]
    pub enum Action {
        /// Normal (should be computed) + limit.
        MoveFor { x: f32, y: f32, f: For },
        /// How long it will stay.
        Stay(u64, PausableTimer),
        /// Go to another index. Allows to do a loop.
        Goto(Index),
    }

    impl From<dto::Action> for Action {
        fn from(dto: dto::Action) -> Self {
            match dto {
                dto::Action::MoveFor(degrees, f) => {
                    let (y, x) = degrees.to_radians().sin_cos();
                    Action::MoveFor { x, y, f: f.into() }
                }
                dto::Action::Stay(millis) => Action::Stay(millis, PausableTimer::new(0)),
                dto::Action::Goto(i) => Action::Goto(i),
            }
        }
    }

    #[derive(Debug, Clone)]
    pub enum For {
        /// Pixels.
        Distance { base: f32, current: f32 },
        /// Milliseconds.
        Duration { millis: u64, timer: PausableTimer },
    }

    impl From<dto::For> for For {
        fn from(dto: dto::For) -> Self {
            match dto {
                dto::For::Duration(millis) => For::Duration {
                    millis,
                    timer: PausableTimer::new(0),
                },
                dto::For::Distance(base) => For::Distance { base, current: 0. },
            }
        }
    }

    /// Allows to specify to which action Goto sends.
    #[derive(serde::Deserialize, Debug, Clone)]
    pub enum Index {
        Absolute(usize),
        Relative(isize),
    }
}
