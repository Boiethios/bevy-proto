use crate::game_area::{
    time::Paused,
    weapons::{Level, Weapon},
    Speed,
};
use bevy::prelude::*;

pub struct Ship {
    pub key_up: KeyCode,
    pub key_down: KeyCode,
    pub key_left: KeyCode,
    pub key_right: KeyCode,
    pub key_fire: KeyCode,
}

pub mod system {
    use super::*;

    pub fn setup(
        mut commands: Commands,
        textures: Res<crate::TextureHandles>,
        weapons_descriptor: Res<crate::game_area::weapons::Descriptor>,
    ) {
        commands
            .spawn(SpriteComponents {
                material: textures.ship,
                transform: crate::translation(800., 50., 0.),
                ..Default::default()
            })
            .with(Speed(500.))
            .with(Ship {
                key_up: KeyCode::Up,
                key_down: KeyCode::Down,
                key_left: KeyCode::Left,
                key_right: KeyCode::Right,
                key_fire: KeyCode::Space,
            })
            .with_children(|parent| {
                parent
                    .spawn(
                        weapons_descriptor
                            .get_cannon(Level::One)
                            .to_components(Level::One),
                    )
                    .with(Weapon);
            });
    }

    pub fn movement(
        time: Res<Time>,
        paused: Res<Paused>,
        keyboard_input: Res<Input<KeyCode>>,
        mut ships: Query<(&Ship, &Children, &Speed, &mut Transform)>,
        weapon: Query<With<Weapon, &mut Level>>,
    ) {
        if paused.yes() {
            return;
        }

        for (ship, children, speed, mut transform) in &mut ships.iter() {
            let mut x_direction = 0.0;
            if keyboard_input.pressed(ship.key_left) {
                x_direction -= 1.0;
            }
            if keyboard_input.pressed(ship.key_right) {
                x_direction += 1.0;
            }
            let mut y_direction = 0.0;
            if keyboard_input.pressed(ship.key_down) {
                y_direction -= 1.0;
            }
            if keyboard_input.pressed(ship.key_up) {
                y_direction += 1.0;
            }

            // DEBUG STUFF: allows to enhance the weapon:
            if keyboard_input.just_pressed(KeyCode::Equals) {
                for child in children.iter() {
                    if let Ok(mut level) = weapon.get_mut::<Level>(*child) {
                        level.inc();
                    }
                }
            }

            let s = time.delta_seconds * speed.0;
            let translation = transform.translation_mut();

            *translation.x_mut() = (translation.x() + s * x_direction).max(200.).min(1400.);
            *translation.y_mut() = (translation.y() + s * y_direction).max(0.).min(900.);
        }
    }
}
