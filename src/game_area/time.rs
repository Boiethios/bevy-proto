use bevy::prelude::*;
use std::ops::Not;
use std::time::Duration;

/// Ressource handling the pause state.
#[derive(Clone, Copy)]
pub struct Paused(bool);

impl Paused {
    pub fn yes(self) -> bool {
        self.0
    }
    pub fn no(self) -> bool {
        self.0.not()
    }
    fn switch(&mut self) {
        self.0 = self.0.not()
    }
}

#[derive(Debug, Clone)]
pub struct PausableTimer(Timer);

impl PausableTimer {
    pub fn new(millis: u64) -> Self {
        Self(Timer::new(Duration::from_millis(millis), false))
    }

    pub fn finished(&self) -> bool {
        self.0.finished
    }

    pub fn reset(&mut self) {
        self.0.reset()
    }

    pub fn tick(&mut self, time: &Time) {
        self.0.tick(time.delta_seconds)
    }
}

pub mod system {
    use super::*;

    pub fn setup(mut commands: Commands) {
        commands.insert_resource(Paused(false));
    }

    pub fn update(
        time: Res<Time>,
        keyboard_input: Res<Input<KeyCode>>,
        mut paused: ResMut<Paused>,
        mut timers: Query<&mut PausableTimer>,
    ) {
        let game_speed = 1.;

        if keyboard_input.just_pressed(KeyCode::P) {
            paused.switch()
        }

        if paused.no() {
            for mut timer in &mut timers.iter() {
                timer.0.tick(time.delta_seconds * game_speed)
            }
        }
    }

    /// Reset all the pausable timers.
    pub fn reset(mut timer: Mut<PausableTimer>) {
        timer.reset()
    }
}
