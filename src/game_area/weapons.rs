pub mod cannon;

use bevy::prelude::*;

/// A player's weapon.
pub struct Weapon;

/// A resource that describe the weapons parameters.
pub struct Descriptor {
    pub cannon: Vec<cannon::CannonDescriptor>,
}

impl Default for Descriptor {
    fn default() -> Self {
        let cannon = ron::de::from_reader(crate::data::weapon("cannon"))
            .expect("Cannot deserialize the cannon descriptor");

        Descriptor { cannon }
    }
}

impl Descriptor {
    pub fn get_cannon(&self, level: Level) -> &cannon::CannonDescriptor {
        self.cannon.get(level as usize - 1).expect(&format!(
            "Weapon level should exist: cannon: {}",
            level as u8
        ))
    }
}

#[derive(Clone, Copy)]
pub enum Level {
    One = 1,
    Two,
    Three,
    Four,
}

impl Level {
    pub fn new(level: u8) -> Option<Self> {
        match level {
            1 => Some(Level::One),
            2 => Some(Level::Two),
            3 => Some(Level::Three),
            4 => Some(Level::Four),
            _ => None,
        }
    }

    pub fn inc(&mut self) {
        *self = match *self {
            Level::One => Level::Two,
            Level::Two => Level::Three,
            Level::Three => Level::Four,
            Level::Four => Level::One,
        };
    }
}
