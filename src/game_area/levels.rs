use super::{
    monsters::{
        self,
        behavior::{dto, Behavior},
    },
    time::PausableTimer,
};
use bevy::prelude::*;

#[derive(serde::Deserialize, Debug)]
struct SpawningMonster {
    time: u64,
    kind: monsters::Type,
    place: f32,
    actions: Vec<dto::Action>,
}

#[derive(serde::Deserialize)]
pub struct Level {
    name: String,
    monsters: Vec<SpawningMonster>,
}

pub mod system {
    use super::*;

    pub fn setup(mut commands: Commands) {
        let level: Level = ron::de::from_reader(crate::data::level("level-1"))
            .expect("Cannot deserialize the cannon descriptor");
        let name = level.name.clone();

        for monster in dbg!(level.monsters) {
            match monster.kind {
                monsters::Type::Spider(spider) => commands.spawn((
                    spider,
                    monster.place,
                    PausableTimer::new(monster.time),
                    Behavior::new(monster.actions.into_iter().map(Into::into).collect()),
                )),
            };
        }

        println!("Spawned level: {}", name);
    }
}
