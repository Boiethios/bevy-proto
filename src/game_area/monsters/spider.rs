use crate::game_area::{
    monsters::behavior::{Action, Behavior, For, Index},
    time::{PausableTimer, Paused},
    Speed,
};
use bevy::prelude::*;

#[derive(serde::Deserialize, Clone, Debug)]
pub struct Spider {
    //
}

/// The speed of the spider.
const SPEED: Speed = Speed(200.);

pub mod system {
    use super::*;

    pub fn spawn(
        mut commands: Commands,
        textures: Res<crate::TextureHandles>,
        entity: Entity,
        x: &f32,
        spider: &Spider,
        timer: &PausableTimer,
        behavior: &Behavior,
    ) {
        if timer.finished() {
            commands
                .despawn(entity)
                .spawn(SpriteComponents {
                    material: textures.spider,
                    transform: crate::translation(*x, 920., 0.),
                    ..Default::default()
                })
                .with(spider.clone())
                .with(SPEED)
                .with(behavior.clone());
        }
    }

    //TODO let it be agnostic (monster instead of spider)
    pub fn update(
        mut commands: Commands,
        time: Res<Time>,
        paused: Res<Paused>,
        entity: Entity,
        _spider: &Spider,
        speed: &Speed,
        mut behavior: Mut<Behavior>,
        mut transform: Mut<Transform>,
    ) {
        if paused.yes() {
            return;
        }

        if behavior.is_new() {
            match behavior.action_mut() {
                Some(Action::MoveFor {
                    x: _,
                    y: _,
                    f: For::Distance { base, current },
                }) => *current = *base,
                Some(Action::MoveFor {
                    x: _,
                    y: _,
                    f: For::Duration { millis, timer },
                }) => *timer = PausableTimer::new(*millis),
                Some(Action::Stay(millis, timer)) => *timer = PausableTimer::new(*millis),
                Some(Action::Goto(_)) => unreachable!("TODO"),
                None => (),
            }
            behavior.disable_new();
        }

        match behavior.action_mut() {
            // If there is no current action, the monster just goes forward
            // until it goes out of the screen and despawns.
            None => {
                *transform.translation_mut().y_mut() -= speed.0 * time.delta_seconds;
            }
            Some(Action::MoveFor {
                x,
                y,
                f: For::Distance { base: _, current },
            }) => {
                *transform.translation_mut().x_mut() += *x * time.delta_seconds * speed.0;
                *transform.translation_mut().y_mut() += *y * time.delta_seconds * speed.0;
                *current -= time.delta_seconds * speed.0;
                if *current <= 0. {
                    behavior.next();
                }
            }
            Some(Action::MoveFor {
                x,
                y,
                f: For::Duration { millis: _, timer },
            }) => {
                *transform.translation_mut().x_mut() += *x * time.delta_seconds * speed.0;
                *transform.translation_mut().y_mut() += *y * time.delta_seconds * speed.0;
                timer.tick(&*time);
                if timer.finished() {
                    behavior.next();
                }
            }
            Some(Action::Stay(_millis, timer)) => {
                timer.tick(&*time);
                if timer.finished() {
                    behavior.next();
                }
            }
            Some(Action::Goto(_)) => unreachable!("Action::Goto should have been treated before"),
        };

        if transform.translation_mut().x() < -20. {
            commands.despawn(entity);
        }
    }
}
