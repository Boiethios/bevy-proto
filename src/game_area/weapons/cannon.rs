use super::{Descriptor, Level};
use crate::game_area::{
    time::{PausableTimer, Paused},
    Ship, Speed,
};
use bevy::prelude::*;

/// One of the ship's weapons.
#[derive(Default)]
pub struct Cannon {
    state: bool,
}

/// The cannon bundle.
#[derive(Bundle)]
pub struct CannonComponents {
    cannon: Cannon,
    level: Level,
    timer: PausableTimer,
}

#[derive(serde::Deserialize)]
pub struct CannonDescriptor {
    ticks: u64,
    state_1: Vec<Vec3>,
    state_2: Vec<Vec3>,
}

impl CannonDescriptor {
    pub fn to_components(&self, level: Level) -> CannonComponents {
        CannonComponents {
            cannon: Cannon::default(),
            level,
            timer: PausableTimer::new(self.ticks),
        }
    }
}

/// A projectile fired by the cannon.
pub struct Bullet;

pub mod system {
    use super::*;
    use crate::TextureHandles;

    /// Creates a new bullet if the fire key is pressed and the fire pace allows it.
    pub fn fire(
        mut commands: Commands,
        textures: Res<TextureHandles>,
        paused: Res<Paused>,
        weapons_descriptor: Res<Descriptor>,
        keyboard_input: Res<Input<KeyCode>>,
        ships: Query<(&Ship, &Transform)>,
        mut cannons: Query<(&Parent, &mut Cannon, &mut PausableTimer, &Level)>,
    ) {
        if paused.yes() {
            return;
        }

        for (ship_id, mut cannon, mut timer, level) in &mut cannons.iter() {
            let ship = ships.get::<Ship>(ship_id.0).unwrap();
            if keyboard_input.pressed(ship.key_fire) && timer.finished() {
                let origin = ships.get::<Transform>(ship_id.0).unwrap();

                timer.reset();

                let descriptor = weapons_descriptor.get_cannon(*level);
                let bullet_translations = if cannon.state {
                    &descriptor.state_1
                } else {
                    &descriptor.state_2
                }
                .iter()
                .copied();

                for translation in bullet_translations {
                    let mut transform = (*origin).clone();
                    transform.translate(translation);
                    commands
                        .spawn(SpriteComponents {
                            material: textures.cannon_bullet,
                            transform,
                            ..Default::default()
                        })
                        .with(Bullet)
                        .with(Speed(1000.));
                }
                cannon.state = !cannon.state;
            }
        }
    }

    /// Update the bullets position each frame.
    pub fn update(
        mut commands: Commands,
        time: Res<Time>,
        paused: Res<Paused>,
        mut query: Query<With<Bullet, (Entity, &Speed, &mut Transform)>>,
    ) {
        if paused.yes() {
            return;
        }

        for (entity, speed, mut transform) in &mut query.iter() {
            let translation = transform.translation_mut();
            if translation.y() >= 905. {
                commands.despawn(entity);
            } else {
                *translation.y_mut() += time.delta_seconds * speed.0;
            }
        }
    }
}
