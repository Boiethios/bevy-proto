//! Centralized place allowing to load data.
//!
//! Every of those functions will panic if the resource isn't present, since it's
//! not an error that should happen.

use bevy::prelude::*;
use std::{fs::File, path::PathBuf};

const DATA: &str = "data";

pub fn weapon(name: &str) -> std::fs::File {
    let path = PathBuf::from(DATA)
        .join("weapons")
        .join(name)
        .with_extension("ron");

    File::open(&path).expect(&format!("Failed to open the resource: {:?}", path))
}

pub fn level(name: &str) -> std::fs::File {
    let path = PathBuf::from(DATA)
        .join("levels")
        .join(name)
        .with_extension("ron");

    File::open(&path).expect(&format!("Failed to open the resource: {:?}", path))
}

pub fn asset(name: &str, server: &AssetServer) -> ColorMaterial {
    let path = PathBuf::from("assets").join(name);

    server
        .load(&path)
        .expect(&format!("Failed to open the asset: {:?}", path))
        .into()
}
