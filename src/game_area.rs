//! The actual game, with the ships, shots, power-ups, etc.

mod levels;
mod monsters;
mod ship;
mod time;
mod weapons;

use bevy::prelude::*;

pub use ship::Ship;

/// Indicates that something (a weapon, a bullet, etc.) is owned by the player.
/// Query `Without<Player, T>` to get exclusively a monster's entity.
pub struct Player;

pub struct Plugin;

impl bevy::prelude::Plugin for Plugin {
    fn build(&self, app: &mut AppBuilder) {
        app.add_resource(weapons::Descriptor::default())
            // Level
            .add_startup_system(levels::system::setup.system())
            // Shot
            .add_system(weapons::cannon::system::fire.system())
            .add_system(weapons::cannon::system::update.system())
            // Ship
            .add_startup_system(ship::system::setup.system())
            .add_system(ship::system::movement.system())
            // Monster
            .add_system(monsters::spider::system::spawn.system())
            .add_system(monsters::spider::system::update.system())
            // Time
            .add_startup_system(time::system::setup.system())
            .add_startup_system_to_stage(bevy::app::startup_stage::POST_STARTUP, time::system::reset.system())
            .add_system_to_stage(stage::PRE_UPDATE, time::system::update.system())
            /*-*/
            ;
    }
}

/// Component that represents a speed used during update movement.
pub struct Speed(f32);
