use bevy::prelude::*;

mod camera;
mod data;
mod game_area;
mod panels;

const W: u32 = 1600;
const H: u32 = (W * 9) / 16; // 900
const INIT: &str = "my_init";

pub struct TextureHandles {
    cannon_bullet: Handle<ColorMaterial>,
    ship: Handle<ColorMaterial>,
    spider: Handle<ColorMaterial>,
}

fn load_textures(
    mut commands: Commands,
    asset_server: Res<AssetServer>,
    mut materials: ResMut<Assets<ColorMaterial>>,
) {
    let s = &*asset_server;

    commands.insert_resource(TextureHandles {
        cannon_bullet: materials.add(data::asset("shot.png", s)),
        ship: materials.add(data::asset("ship.png", s)),
        spider: materials.add(data::asset("spider.png", s)),
    });
}

fn main() {
    App::build()
        .add_resource(WindowDescriptor {
            title: "I am a window!".to_string(),
            width: W,
            height: H,
            vsync: true,
            resizable: false,
            ..Default::default()
        })
        .add_default_plugins()
        .add_startup_stage_before(bevy::app::startup_stage::STARTUP, INIT)
        .add_startup_system_to_stage(INIT, load_textures.system())
        .add_startup_system(camera::system::setup.system())
        .add_plugin(panels::Plugin)
        .add_plugin(game_area::Plugin)
        .run();
}

/// Creates a new translation.
fn translation(x: f32, y: f32, z: f32) -> Transform {
    Transform::from_translation(Vec3::new(x, y, z))
}
