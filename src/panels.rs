//! The left and right part of the window to display the information.

use bevy::prelude::*;

pub struct Plugin;

impl bevy::prelude::Plugin for Plugin {
    fn build(&self, app: &mut AppBuilder) {
        app.add_startup_system(background::setup.system());
    }
}

mod background {
    use bevy::prelude::*;

    pub fn setup(mut commands: Commands, mut materials: ResMut<Assets<ColorMaterial>>) {
        commands.spawn(SpriteComponents {
            material: materials.add(Color::rgb(0.2, 0.2, 0.8).into()),
            transform: crate::translation(100., 450., -0.1),
            sprite: Sprite::new(Vec2::new(200., 900.)),
            ..Default::default()
        });
        commands.spawn(SpriteComponents {
            material: materials.add(Color::rgb(0.2, 0.2, 0.8).into()),
            transform: crate::translation(1500., 450., -0.1),
            sprite: Sprite::new(Vec2::new(200., 900.)),
            ..Default::default()
        });
    }
}
